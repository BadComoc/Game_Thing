#include "Game.h"

#include "Square.h"

Square square2 = Square();
Square square1 = Square();

Game::Game(const char* GAME_TITLE, const int SCREEN_WIDTH, const int SCREEN_HEIGHT, float BACKGROUND_R, float BACKGROUND_G, float BACKGROUND_B, float BACKGROUND_A)
{
	if (Init(GAME_TITLE, SCREEN_WIDTH, SCREEN_HEIGHT) != GAME_UNSUCCEEDED) // Call Init Function.
	{
		square2.Init(1.0f, 1.0f, "Resources/Shaders/square2.vert", "Resources/Shaders/square2.frag", "Square2");
		square1.Init(0.5f, 0.5f, "Resources/Shaders/square1.vert", "Resources/Shaders/square1.frag", "Square1");

		// Main Game Loop.
		gameRunning = true;
		while (gameRunning)
		{
			Update(calculateDeltaTime());

			glClearColor(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B, BACKGROUND_A);
			glClear(GL_COLOR_BUFFER_BIT);

			Draw();

			SDL_GL_SwapWindow(gWindow);
		}
	}
	else
	{
		Close();
	}
}

bool Game::Init(const char* GAME_TITLE, const int SCREEN_WIDTH, const int SCREEN_HEIGHT)
{
	// Game Initialization.
	int success;

	std::cout << "Game Initializing..." << std::endl;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		std::cout << "SDL could not be initialized! ERROR: " << SDL_GetError() << std::endl;
		success = GAME_UNSUCCEEDED;
	}
	else
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, GL_TRUE);

		gWindow = SDL_CreateWindow(GAME_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
		if (gWindow == nullptr)
		{
			std::cout << "Window could not be created! ERROR: " << SDL_GetError() << std::endl;
			Close();
			success = GAME_UNSUCCEEDED;
		}
		else
		{
			gContext = SDL_GL_CreateContext(gWindow);

			if (!gladLoadGL())
			{
				std::cout << "OpenGL could not be Initialized!" << std::endl;
				Close();
				success = GAME_UNSUCCEEDED;
			}
			else
			{
				glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

				std::cout << "Finished Initalizing..." << std::endl;
				success = GAME_SUCCEEDED;
			}
		}
	}

	return success;
}

float Game::calculateDeltaTime()
{
	currentTime = (float)SDL_GetTicks();
	deltaTime = currentTime - lastTime;
	lastTime = currentTime;

	return deltaTime;
}

bool toggle = false;
void Game::Update(GLfloat deltaTime)
{
	// Update and Inputs.
	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
		{
			Close();
		}
		else if (e.type == SDL_KEYDOWN)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				Close();
				break;
			case SDLK_a:
				toggle = !toggle;
				if (toggle == true)
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				}
				else
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				}
				break;
			}
		}
	}

	//printFloat(deltaTime);
}

void Game::Draw()
{
	// Draw Stuff here.
	square2.Draw(0.0f, 0.5f, 0.0f, 1.0f, "Square2");
	square1.Draw(0.5f, 0.0f, 0.0f, 1.0f, "Square1");
}

void Game::Close()
{
	// Game Close.
	std::cout << "Game Exiting..." << std::endl;

	gameRunning = false;

	SDL_GL_DeleteContext(gContext);
	SDL_DestroyWindow(gWindow);
	gContext = nullptr;
	gWindow = nullptr;

	SDL_Quit();
}
