#pragma once

// C++ and Game Includes.
#include <iostream>

#include <SDL.h> // Input/etc Library.
#include <glad/glad.h> // OpenGL Wrangler.

// Other Includes.
#include "ResourceManager.h" // Manages Shaders, Textures, etc.

// Represents the current state of the game.
enum GameState
{
	GAME_ACTIVE,
	GAME_MENU
};

class Game
{
public:
	// Constructors.
	Game() = default;
	Game(const char* GAME_TITLE, const int SCREEN_WIDTH, const int SCREEN_HEIGHT, float BACKGROUND_R, float BACKGROUND_G, float BACKGROUND_B, float BACKGROUND_A);

	// Game Variables.
	bool gameRunning = false; // Boolean for the Main Game Loop.
	GameState state = GAME_ACTIVE;

	// Delta Time.
	float currentTime = 0;
	float deltaTime = 0;
	float lastTime = 0;
	float calculateDeltaTime();

	// Main Game Functions.
	bool Init(const char* GAME_TITLE, const int SCREEN_WIDTH, const int SCREEN_HEIGHT);
	void Update(GLfloat deltaTime);
	void Draw();
	void Close();

private:
	// Misc Defines.
	#define GAME_SUCCEEDED 1
	#define GAME_UNSUCCEEDED 0

	// Mandatory Game Variables.
	SDL_Window* gWindow = nullptr;
	SDL_GLContext gContext = nullptr;

	SDL_Event e;

	// Misc Console Printing Functions.
	void print(char* text)
	{
		std::cout << text << std::endl;
	}
	void printFloat(float text)
	{
		std::cout << text << std::endl;
	}
	void printInt(int text)
	{
		std::cout << text << std::endl;
	}

};
