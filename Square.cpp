#include "Square.h"

void Square::Init(float sizeX, float sizeY, const char* vertPath, const char* fragPath, const char* shaderName)
{
	shaders = ResourceManager::LoadShader(vertPath, fragPath, nullptr, shaderName);

	GLfloat vertices[] =
	{
		sizeX, sizeY, 0.0f,
		sizeX, -sizeY, 0.0f,
		-sizeX, -sizeY, 0.0f,
		-sizeX, sizeY, 0.0f
	};
	GLuint indices[] =
	{
		0, 1, 3,
		1, 2, 3
	};

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
}

void Square::Draw(float colorR, float colorG, float colorB, float colorA, const char* shaderName)
{
	int vertexColorLocation = glGetUniformLocation(ResourceManager::GetShader(shaderName).ID, "vertColor");
	
	ResourceManager::GetShader(shaderName).Use();

	glUniform4f(vertexColorLocation, colorR, colorG, colorB, colorA);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
