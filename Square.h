#pragma once

#include <SDL.h>
#include <glad/glad.h>

#include "ResourceManager.h"

class Square
{
public:
	Square() = default;

	Shader shaders;

	void Init(float sizeX, float sizeY, const char* vertPath, const char* fragPath, const char* shaderName);
	void Draw(float colorR, float colorG, float colorB, float colorA, const char* shaderName);

private:
	GLuint VAO, VBO, EBO;

};
