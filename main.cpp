#include <iostream>

#include <SDL.h>
#include <glad/glad.h>

#include "Game.h"

// Game Class.
Game game;

int main(int argc, char* argv[])
{
	// Initialize Game.
	game = Game("Game", 600, 400, 0.2f, 0.3f, 0.3f, 1.0f);

	return 0;
}
